import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class GameOfLifeTest {

    @Test
    public void shouldBeTrueToFalseFor1x1True() {
        // Arrange
        boolean[][] initialGameState = new boolean[1][1];
        initialGameState[0][0] = true;
        GameOfLife gameOfLife = new GameOfLife(initialGameState);
        // Act
        boolean[][] afterState = gameOfLife.getNextGameState();
        // Assert
        boolean[][] expectedGameState = new boolean[1][1];
        expectedGameState[0][0] = false;
        assertArrayEquals(expectedGameState, afterState);
    }


    @Test
    public void shouldBe3x3AllFalseFor3x3AllFalse() {
        // Arrange
        boolean[][] initialGameState = new boolean[][]{
            { false, false, false},
            { false, false, false},
            { false, false, false}
        };
        GameOfLife gameOfLife = new GameOfLife(initialGameState);
        // Act
        boolean[][] afterState = gameOfLife.getNextGameState();
        // Assert
        boolean[][] expectedGameState = new boolean[][]{
                { false, false, false},
                { false, false, false},
                { false, false, false}
        };
        assertArrayEquals(expectedGameState, afterState);
    }
}
