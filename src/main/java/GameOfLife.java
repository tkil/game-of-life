
public class GameOfLife {
    private boolean[][] gameState;
    private int height;
    private int width;

    public GameOfLife(boolean[][] initialGameState) {
        if(initialGameState.length == 0) {
            throw new IllegalArgumentException("Must have array length greater than zero for height");
        }
        if(initialGameState[0].length == 0) {
            throw new IllegalArgumentException("Must have array length greater than zero for width");
        }
        gameState = initialGameState.clone();
        height = initialGameState.length;
        width = initialGameState[0].length;
    }

    public boolean[][] getNextGameState() {
        boolean[][] nextState = new boolean[height][width];
        for(int row=0; row<height; row++) {
            for(int col=0; col<width; col++) {
                nextState[row][col] = false;
            }
        }
        gameState = nextState;
        return gameState;
    }
}
